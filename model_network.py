import datetime
from data_prepare import train_db, test_db, normed_train_db, normed_test_db, train_labels, test_lables
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers,losses
import tensorboard as tb
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib

'''实现自定义神经网络模型进行预测'''

matplotlib.rcParams['font.family'] = 'STsong'  # 修改全局变量
matplotlib.rcParams['font.size'] = 12


class Network(keras.Model):
    # 回归网络模型
    def __init__(self):
        super(Network, self).__init__()
        # 创建3个全连接层
        self.fc1 = layers.Dense(64, activation='relu')  # 第一层输出节点为64
        self.fc2 = layers.Dense(64, activation='relu')  # 第二层输出节点为64
        self.fc3 = layers.Dense(1)  # 预测类，只有一个输出节点，并且实数范围内不需要激活函数

    def call(self, inputs, training=None, mask=None):
        # 实现网络逻辑，依次通过3个全连接层
        x = self.fc1(inputs)
        x = self.fc2(x)
        x = self.fc3(x)

        return x

# 类创建网络类实例
model = Network()

# 通过build函数完成内部张量的创建，其中4为batch数，9为输入特征的长度
model.build(input_shape=(4, 9))

model.summary()  # 打印网络信息

optimizer = tf.keras.optimizers.SGD(0.01)  # 创建优化器，指定学习率

# 创建监控类，监控数据将写入log_dir目录
# current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
# log_dir = 'logs/' + current_time
# summary_writer = tf.summary.create_file_writer(log_dir)

# 从训练数据中批量抽取10条例子用于预测
# example_batch = normed_train_db[:10]
# example_result = model.predict(example_batch)
# print(example_result)


loss_log = []  # 用于记录loss的变化
i = 0
# 网络训练
for epoch in range(50):  # 50个Epoch
    for step, (x, y) in enumerate(train_db):  # 遍历一次训练集
        # 梯度记录器，训练时需要使用它
        with tf.GradientTape() as tape:
            out = model(x)
            loss = tf.reduce_mean(losses.MSE(y, out))  # 计算MSE
            mae_loss = tf.reduce_mean(losses.MAE(y, out))  # 计算MAE

        # if step % 10 == 0:
        #     # print("下标step:", step)
        #     print(epoch, step, float(mae_loss), float(loss))
            # tensorboard监控
            # with summary_writer.as_default():  # 写入环境
            #     # 当前时间戳step上的数据为loss，写入到名为train-loss数据库中
            #     tf.summary.scalar('train-loss', float(loss), step=step)
        i += 1
        loss_log.append([i, float(loss)])
        # 计算梯度并更新
        grads = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(grads, model.trainable_variables))


loss_log_df = pd.DataFrame(loss_log, columns=['epoch', 'MSE'])
epoch = loss_log_df['epoch']
mse = loss_log_df['MSE']
plt.plot(epoch, mse)
plt.xlabel("训练轮数(epoch)")
plt.ylabel("误差(MSE)")
plt.show()

# 测试集的结果
# out = model(normed_test_db.values)
# test_mse = tf.reduce_mean(losses.MSE(test_lables, out))
#
# print(test_mse)

# 从测试数据中批量抽取10条例子用于预测
example_batch = normed_test_db.values[:10]
example_result = model.predict(example_batch)
print(example_result)
