from data_prepare import normed_train_db, normed_test_db, train_labels
from pandas.plotting import scatter_matrix
import matplotlib.pyplot as plt
from matplotlib.pyplot import scatter
import seaborn as sns
import numpy as np

attributes = {
    'Cylinders',
    'Displacement',
    'Horsepower',
    'Weight',
    'Acceleration'
}

scatter_matrix(normed_train_db[attributes], figsize=(12, 8))

plt.show()