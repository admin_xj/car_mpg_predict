import tensorflow as tf
from tensorflow import keras
import pandas as pd

'''利用全连接神经网络模型来完成汽车的效能指标的预测问题'''

# 在线下载汽车效能数据集
dataset_path = keras.utils.get_file("auto-mpg.data",
                                    "http://archive.ics.uci.edu/ml/machine-learning-databases/auto-mpg/auto-mpg.data")

# 数据集存储路径
# dataset_path = 'C:\\Users\Administrator\.keras\datasets\\auto-mpg.data'

def data_pre_clean(data_path):
    '''
    进行数据处理
    :param data_path: 文件存储路径，格式为DAT
    :return: 适用于神经网络模型的数据格式,train_db,test_db
    '''
    dataset_path  = data_path

    # 利用pandas读取数据集，字段有效能(公里数每加仑)、气缸数、排量、马力、重量、加速度、型号年份、产地
    column_names = ['MPG', 'Cylinders', 'Displacement', 'Horsepower', 'Weight', 'Acceleration', 'Model Year', 'Origin']

    raw_dataset = pd.read_csv(dataset_path, names=column_names,
                              na_values="?", comment='\t',
                              sep=" ", skipinitialspace=True)
    dataset = raw_dataset.copy()

    # 查看部分数据
    print("原始数据: ")
    print(dataset.head())

    # 数据清洗，删除含有缺失值的数据项
    # dataset = pd.isna()  # 统计空白数据项
    dataset = dataset.dropna()  # 删除空白数据项
    # dataset.isna.sum()

    # 处理类别数据，将1,2,3代表的类别变为新增的三个字段
    origin = dataset.pop('Origin')  # 先弹出origin这一列,删除并返回
    # 根据origin列来写入新的3个列
    dataset['USA'] = (origin == 1) * 1.0
    dataset['Europe'] = (origin == 2) * 1.0
    dataset['Japan'] = (origin == 3) * 1.0

    print("编码后的数据： ")
    print(dataset.tail())  # 查看新表格后几项

    # 8:2划分为测试集与训练集:
    train_dataset = dataset.sample(frac=0.8, random_state=0)  # 随机抽取函数，取数不得重复
    test_dateset = dataset.drop(train_dataset.index)  # 存放删除后的数据

    # 将MPG字段移出为标签数据
    train_labels = train_dataset.pop('MPG')
    test_labels = test_dateset.pop('MPG')

    # 对数据进行标准化
    train_stats = train_dataset.describe()  # 查看输入数据x
    # train_stats.pop('MPG')
    print("数据的统计量：")
    print(train_stats)
    train_stats = train_stats.transpose()  # 转置
    normed_train_data = norm(train_dataset, train_stats)  # 标准化训练集
    normed_test_data = norm(test_dateset, train_stats)  # 标准化测试集

    # 打印出训练集和测试集的大小
    print(normed_train_data.shape, train_labels.shape)
    print(normed_test_data.shape, test_labels.shape)

    # 利用切分的训练集数据构建训练数据集对象:
    train_db = tf.data.Dataset.from_tensor_slices((normed_train_data.values,train_labels.values))
    train_db = train_db.shuffle(100).batch(32)  # 随机打散，批量化

    # 构建测试数据集对象
    test_db = tf.data.Dataset.from_tensor_slices(((normed_test_data.values, test_labels.values)))
    test_db = test_db.shuffle(100).batch(32)
    return normed_train_data, train_labels, normed_test_data, test_labels, train_db, test_db

def norm(x, train_stats):  # 减去每个字段的均值，并除以标准差
    return (x - train_stats['mean']) / train_stats['std']

normed_train_db, train_labels, normed_test_db, test_lables, train_db, test_db = data_pre_clean(dataset_path)
